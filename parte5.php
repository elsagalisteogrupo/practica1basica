<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    
    <body>
        <div style ="width:50px;height:50px;background-color:<?=calculoColor()?>"></div>
        
        <?php
        function calculoColor(){
            $color="rgb(" . rand(0,255) . " , " . rand(0,255) . ")";
            return $color;
        }
        
        $color = calculoColor();
       
        ?>
        
        <p>Color: <?=$color ?></p>
        
        <svg version="1.1"xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" style="display:block;margin:0px auto;">
        <circle cx="50" cy="50" fill="<?=$color ?>"/>
        </svg>
    </body>
</html>
